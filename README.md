# SimpleMarks #

### Integrantes ###

* Arcila Laguna, Sergio Andres
* Puig Verastegui, Diana Carolina

### Centro educativo ###

* IES Las Galletas
* 2020/2021
* 2º Desarrollo de Aplicaciones Web (DAW)

### Descripción ###

El proyecto consiste en el desarrollo de una aplicación web destinada a los docentes con dificultades para utilizar las nuevas tecnologías donde, a través de una interfaz simple e intuitiva, puedan realizar el cálculo de las evaluaciones del alumnado.

### Implementación ###

Mediante el uso de Heroku, hemos lanzado la aplicación para poder probar la página desde la web.

* [SimpleMarks](https://simple-marks.herokuapp.com/)