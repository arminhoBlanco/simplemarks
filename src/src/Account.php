<?php

namespace Project;

use PDO;
use PDOException;

class Account extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAccount($emailAccount)
    {
        $stmt = $this->link->prepare('SELECT * FROM accounts WHERE email = ?');
        $stmt->bindParam(1, $emailAccount);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al recuperar productos: ' . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function createAccount($emailAccount, $passAccount)
    {
        $stmt = $this->link->prepare('INSERT INTO `accounts`(`email`, `password`) VALUES (?,?)');
        $stmt->bindParam(1, $emailAccount);
        $stmt->bindParam(2, $passAccount);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al crear la cuenta: ' . $ex->getMessage());
        }
        return [$emailAccount, $passAccount];
    }
}
