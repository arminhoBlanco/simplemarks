<?php

namespace Project;

use PDO;
use PDOException;

class Matter extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllMattersYear($idAccount, $idYear)
    {
        $stmt = $this->link->prepare('SELECT * FROM matters WHERE id_accounts = ? AND id_school_years = ?');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al recuperar las materias: ' . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function getNameMatter($idAccount, $idYear, $idMatter)
    {
        $stmt = $this->link->prepare('SELECT name, grade FROM matters WHERE id_accounts = ? AND id_school_years = ? AND id = ?');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        $stmt->bindParam(3, $idMatter);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al recuperar la materia: ' . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ)[0];
    }

    public function createMatter($newMatter, $idAccount, $idYear)
    {
        $stmt = $this->link->prepare('INSERT INTO `matters`(`id_accounts`, `id_school_years`, `name`, `grade`) VALUES (?,?,?,?)');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        $stmt->bindParam(3, $newMatter['matter_name']);
        $stmt->bindParam(4, $newMatter['matter_grade']);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al crear la nueva materia: " . $ex->getMessage());
        }
    }

    public function deleteMatter($idAccount, $idYear, $idMatter)
    {
        $stmt = $this->link->prepare('DELETE FROM `matters` WHERE `id_accounts` = ? AND `id_school_years` = ? AND `id` = ?');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        $stmt->bindParam(3, $idMatter);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al borrar la : " . $ex->getMessage());
        }
    }
}
