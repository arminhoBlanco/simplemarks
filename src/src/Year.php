<?php

namespace Project;

use PDO;
use PDOException;

class Year extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllYears($idAccount)
    {
        $stmt = $this->link->prepare('SELECT * FROM school_years WHERE id_accounts = ?');
        $stmt->bindParam(1, $idAccount);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al recuperar los años escolares: ' . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function getNameYears($idAccount, $idYear)
    {
        $stmt = $this->link->prepare('SELECT name, college FROM school_years WHERE id_accounts = ? AND id = ?');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al recuperar el años escolar: ' . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ)[0];
    }

    public function createYear($newYear, $idAccount)
    {
        $stmt = $this->link->prepare('INSERT INTO `school_years`(`id_accounts`, `name`, `college`) VALUES (?,?,?)');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $newYear['year_name']);
        $stmt->bindParam(3, $newYear['year_college']);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al crear el nuevo año escolar: " . $ex->getMessage());
        }
    }

    public function deleteYear($idYear, $idAccount)
    {
        $stmt = $this->link->prepare('DELETE FROM `school_years` WHERE `id` = ? AND `id_accounts` = ?');
        $stmt->bindParam(1, $idYear);
        $stmt->bindParam(2, $idAccount);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al borrar el año escolar: " . $ex->getMessage());
        }
    }
}
