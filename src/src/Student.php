<?php

namespace Project;

use PDO;
use PDOException;

class Student extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllStudentsYear($idAccount, $idYear)
    {
        $stmt = $this->link->prepare('SELECT * FROM students WHERE id_accounts = ? AND id_school_years = ? ORDER BY last_name ASC');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al recuperar a los estudiantes: ' . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function getInfoStudent($idAccount, $idYear, $dni)
    {
        $stmt = $this->link->prepare('SELECT * FROM students WHERE id_accounts = ? AND id_school_years = ? AND dni = ?');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        $stmt->bindParam(3, $dni);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die('Error al recuperar al estudiante: ' . $ex->getMessage());
        }
        return $stmt->fetchAll(PDO::FETCH_OBJ)[0];
    }

    public function createStudent($idAccount, $idYear, $newStudent)
    {
        $stmt = $this->link->prepare('INSERT INTO `students`(`dni`, `id_accounts`, `id_school_years`, `first_name`, `last_name`, `grade`) VALUES (?,?,?,?,?,?)');
        $stmt->bindParam(1, $newStudent['student_dni']);
        $stmt->bindParam(2, $idAccount);
        $stmt->bindParam(3, $idYear);
        $stmt->bindParam(4, $newStudent['student_first_name']);
        $stmt->bindParam(5, $newStudent['student_last_name']);
        $stmt->bindParam(6, $newStudent['student_grade']);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al crear al estudiante: " . $ex->getMessage());
        }
    }

    public function deleteStudent($idAccount, $idYear, $dni)
    {
        $stmt = $this->link->prepare('DELETE FROM `students` WHERE `id_accounts` = ? AND `id_school_years` = ? AND `dni` = ?');
        $stmt->bindParam(1, $idAccount);
        $stmt->bindParam(2, $idYear);
        $stmt->bindParam(3, $dni);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            die("Error al borrar al estudiante: " . $ex->getMessage());
        }
    }

    public function importCSV($idAccount, $idYear, $newStudent)
    {
        $stmt = $this->link->prepare('INSERT INTO `students`(`dni`, `id_accounts`, `id_school_years`, `first_name`, `last_name`, `grade`) VALUES (?,?,?,?,?,?)');
        $stmt->bindParam(1, $newStudent[0]);
        $stmt->bindParam(2, $idAccount);
        $stmt->bindParam(3, $idYear);
        $stmt->bindParam(4, $newStudent[1]);
        $stmt->bindParam(5, $newStudent[2]);
        $stmt->bindParam(6, $newStudent[3]);
        try {
            $stmt->execute();
        } catch (PDOException $ex) {
            $_SESSION['message'] = '
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            ' . $ex->getMessage() . '
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            header("Location: ./import_students_form.php");
            exit();
        }
    }
}
