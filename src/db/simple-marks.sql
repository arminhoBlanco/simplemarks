-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-05-2021 a las 21:03:58
-- Versión del servidor: 5.7.31-0ubuntu0.18.04.1
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `simple-marks`
--
CREATE DATABASE IF NOT EXISTS `simple-marks` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `simple-marks`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `accounts`
--

INSERT INTO `accounts` (`id`, `email`, `password`) VALUES
(1, 'eva@ieslasgalletas.net', '$2y$10$cvT0PJm5kJkDIOMJwNiX8OSlN9ScPJnPp0pBrOTDIPurym5oQBlQm'),
(2, 'sergio@ieslasgalletas.net', '$2y$10$cvT0PJm5kJkDIOMJwNiX8OSlN9ScPJnPp0pBrOTDIPurym5oQBlQm');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matters`
--

CREATE TABLE `matters` (
  `id` int(11) NOT NULL,
  `id_accounts` int(11) NOT NULL,
  `id_school_years` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `matters`
--

INSERT INTO `matters` (`id`, `id_accounts`, `id_school_years`, `name`, `grade`) VALUES
(20, 1, 32, '(SSV) Sistemas operativos monopuesto', '1 SMR A'),
(21, 1, 32, '(MJE) Montaje y mantenimiento de equipo', '1 SMR A'),
(22, 1, 32, '(SSN) Sistemas operativos en red', '2 SMR A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `school_years`
--

CREATE TABLE `school_years` (
  `id` int(11) NOT NULL,
  `id_accounts` int(11) NOT NULL,
  `college` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `school_years`
--

INSERT INTO `school_years` (`id`, `id_accounts`, `college`, `name`) VALUES
(32, 1, 'IES Las Galletas', 'Curso 2020/21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--

CREATE TABLE `students` (
  `dni` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_accounts` int(11) NOT NULL,
  `id_school_years` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `students`
--

INSERT INTO `students` (`dni`, `id_accounts`, `id_school_years`, `first_name`, `last_name`, `grade`) VALUES
('00562939Z', 1, 32, 'Kai', 'Deerr', '1SMRA'),
('02255776M', 1, 32, 'Auroora', 'Le Borgne', '1SMRA'),
('02500618N', 1, 32, 'Samuel', 'Bacallado Mendoza', '1SMRA'),
('10752300F', 1, 32, 'Cam', 'Hopkyns', '1SMRA'),
('13852127D', 1, 32, 'Bat', 'Jouannin', '1SMRA'),
('25021903L', 1, 32, 'Leandra', 'Blundel', '1SMRA'),
('38753981Q', 1, 32, 'Arlen', 'Rockliffe', '1SMRA'),
('62682900B', 1, 32, 'Lura', 'Hrynczyk', '1SMRA'),
('63048630H', 1, 32, 'Galina', 'Landsborough', '1SMRA'),
('64718366T', 1, 32, 'Georgetta', 'Losseljong', '1SMRA'),
('68243017Q', 1, 32, 'Sergio Andres', 'Arcila Laguna', '1SMRA'),
('72632884B', 1, 32, 'Sydney', 'MacNeachtain', '1SMRA'),
('73713404Z', 1, 32, 'Erminie', 'Faivre', '1SMRA'),
('74225162E', 1, 32, 'Darrel', 'Mosdell', '1SMRA'),
('76352414G', 1, 32, 'Gabriellia', 'Binden', '1SMRA'),
('83951044D', 1, 32, 'Diana Carolina', 'Puig Be', '1SMRA'),
('84150438Q', 1, 32, 'Mela', 'Schwandt', '1SMRA'),
('90944630P', 1, 32, 'Worthington', 'Makiver', '1SMRA');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `matters`
--
ALTER TABLE `matters`
  ADD PRIMARY KEY (`id`,`id_accounts`,`id_school_years`),
  ADD KEY `matters_ibfk_2` (`id_school_years`),
  ADD KEY `matters_ibfk_1` (`id_accounts`);

--
-- Indices de la tabla `school_years`
--
ALTER TABLE `school_years`
  ADD PRIMARY KEY (`id`,`id_accounts`),
  ADD KEY `id_accounts` (`id_accounts`);

--
-- Indices de la tabla `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`dni`,`id_accounts`,`id_school_years`),
  ADD KEY `students_ibfk_2` (`id_school_years`),
  ADD KEY `students_ibfk_1` (`id_accounts`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `matters`
--
ALTER TABLE `matters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `school_years`
--
ALTER TABLE `school_years`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `matters`
--
ALTER TABLE `matters`
  ADD CONSTRAINT `matters_ibfk_1` FOREIGN KEY (`id_accounts`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matters_ibfk_2` FOREIGN KEY (`id_school_years`) REFERENCES `school_years` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `school_years`
--
ALTER TABLE `school_years`
  ADD CONSTRAINT `school_years_ibfk_1` FOREIGN KEY (`id_accounts`) REFERENCES `accounts` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`id_accounts`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_ibfk_2` FOREIGN KEY (`id_school_years`) REFERENCES `school_years` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
