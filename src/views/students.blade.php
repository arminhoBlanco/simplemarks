@extends('layouts.app')
@section('head')
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
<style>
    .table-hover tbody tr:hover td,
    .table-hover tbody tr:hover th {
        background-color: #AED6F1;
    }
</style>
@endsection
@section('nav')
<span class="w-100 d-lg-none d-block"></span>
<a href="./home.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./logout.php"><i class="fas fa-sign-out-alt"></i>Cerrar sesión</a>
            </div>
        </li>
    </ul>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <a class="btn btn-primary btn-lg btn-block d-md-inline" href="./year.php" role="button">
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h1 class="text-center">({{$year->college}}) - {{$year->name}}</h1>
    </div>
</div>
<hr>
<div class="row mt-4">
    <div class="col-12 col-sm">
        <a class="btn btn-success btn-lg btn-block d-md-inline" href="./student_creation_form.php" role="button">Añadir alumnos</a>
    </div>
    <div class="col-12 mt-2 mt-sm-0 col-sm">
        <div class="text-right">
            <a class="btn btn-warning btn-lg btn-block d-md-inline" href="import_students_form.php" role="button">Importar alumnos</a>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col">
        @if ($message)
        {!! $message !!}
        @endif
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Clase</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($students) > 0)
                    @foreach($students as $i => $student)
                    <tr class="clickable-row" data-href='url://link-for-first-row/'>
                        <th scope="row"><a href="./student.php?id_student={{$student->dni}}">{{$i+1}}</a></th>
                        <td>{{$student->last_name}}</td>
                        <td>{{$student->first_name}}</td>
                        <td>{{$student->grade}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <th scope="row" colspan="4" class="text-center">No hay alumos</th>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('table tr').click(function() {
            var href = $(this).find("a").attr("href");
            if (href) {
                window.location = href;
            }
        });
    });
</script>
@endsection