@extends('layouts.app')
@section('nav')
<span class="w-100 d-lg-none d-block"></span>
<a href="./home.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./logout.php"><i class="fas fa-sign-out-alt"></i>Cerrar sesión</a>
            </div>
        </li>
    </ul>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12">
                <a class="btn btn-primary btn-lg btn-block d-md-inline" href="./home.php" role="button">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col">
        <form action="./year_creation.php" method="POST">
            <div class="form-group">
                <label for="year_college">Colegio</label>
                <input type="text" class="form-control" id="year_college" placeholder="IES Las Galletas" name="year_college" required>
            </div>
            <div class="form-group">
                <label for="year_name">Nombre</label>
                <input type="text" class="form-control" id="year_name" placeholder="Curso 2020/21" name="year_name" required>
            </div>
            <button type=" submit" class="btn btn-primary">Crear</button>
            <button type="reset" class="btn btn-success">Limpiar</button>
            <a href="./home.php" class="btn btn-info" role="button" aria-pressed="true">Volver</a>
        </form>
    </div>
</div>
@endsection