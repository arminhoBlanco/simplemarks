@extends('layouts.app')
@section('nav')
<span class="w-100 d-lg-none d-block"></span>
<a href="./home.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./logout.php"><i class="fas fa-sign-out-alt"></i>Cerrar sesión</a>
            </div>
        </li>
    </ul>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <a class="btn btn-primary btn-lg btn-block d-md-inline" href="./students.php" role="button">
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>
</div>
@if ($message)
{!! $message !!}
@endif
<div class="row">
    <div class="col-12">
        <h1 class="text-center">({{$year->college}}) - {{$year->name}}</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12 mt-4 text-center">
        <a class="btn btn-success btn-lg btn-block d-md-inline mr-5" href="#" role="button">Modificar alumno</a>
        <a class="btn btn-danger btn-lg btn-block d-md-inline" href="./student_delete.php?dni={{$student->dni}}" role="button">Eliminar alumno</a>
    </div>
</div>
<div class="row mt-4 p-4 border border-primary rounded">
    <div class="col-12 col-sm-4 text-center">
        <i class="fas fa-user display-1"></i>
    </div>
    <div class="col-12 col-sm-8 text-center mt-3 mt-sm-0">
        <div class="row text-left h3">
            <div class="col-12 col-sm-6 border-bottom">
                Apellidos:
            </div>
            <div class="col-12 col-sm-6 border-bottom">
                {{$student->last_name}}
            </div>
            <div class="col-12 col-sm-6">
                Nombre:
            </div>
            <div class="col-12 col-sm-6">
                {{$student->first_name}}
            </div>
            <div class="col-12 col-sm-6">
                Curso:
            </div>
            <div class="col-12 col-sm-6">
                {{$student->grade}}
            </div>
        </div>
    </div>
    <div class="col-12 mt-4">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Materia</th>
                        <th scope="col">1 Trimestre</th>
                        <th scope="col">2 Trimestre</th>
                        <th scope="col">3 Trimestre</th>
                        <th scope="col">Final</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row" colspan="5" class="text-center">No hay materias</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection