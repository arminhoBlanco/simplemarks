@extends('layouts.app')
@section('nav')
<span class="w-100 d-lg-none d-block"></span>
<a href="./home.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./logout.php"><i class="fas fa-sign-out-alt"></i>Cerrar sesión</a>
            </div>
        </li>
    </ul>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <a class="btn btn-primary btn-lg btn-block d-md-inline" href="./home.php" role="button">
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h1 class="text-center">({{$year->college}}) - {{$year->name}}</h1>
    </div>
</div>
<hr>
<div class="row mt-5">
    <div class="col-12">
        <a class="btn btn-primary btn-lg btn-block d-md-inline" href="./students.php" role="button">Alumnos</a>
    </div>
</div>
<hr>
<div class="row mt-3">
    <div class="col-6">
        <h2>Materias</h2>
    </div>
    <div class="col-12 col-sm-6 text-right mt-0 mt-md-4">
        <a class="btn btn-success btn-lg btn-block d-md-inline" href="./matter_creation_form.php" role="button">Crear materia</a>
    </div>
    <div class="col-12 mt-3">
        @if ($message)
        {!! $message !!}
        @endif
        @foreach($matters as $matter)
        <div class="card mt-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-7">
                        <a href="#">
                            <h5 class="card-title">{{$matter->grade}} - {{$matter->name}}</h5>
                        </a>
                    </div>
                    <div class="col-5">
                        <a class="btn btn-lg float-right" style="background-color:transparent;" href="./matter_delete.php?id_matter={{$matter->id}}">
                            <i class="fas fa-trash-alt text-danger"></i>
                        </a>
                        <a class="btn btn-lg float-right" style="background-color:transparent;" href="#">
                            <i class="fas fa-cog text-secondary"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection