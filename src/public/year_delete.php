<?php
require '../vendor/autoload.php';

session_start();

use Project\Year;

$year = new Year();

$name = $year->getNameYears($_SESSION['id'], $_GET['id_year']);
$year->deleteYear($_GET['id_year'], $_SESSION['id']);

$_SESSION['message'] = '
<div class="alert alert-success alert-dismissible fade show" role="alert">
    Año escolar (' . $name->college . ') ' . $name->name . '  <strong>eliminado</strong>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>';

header("Location: home.php");
