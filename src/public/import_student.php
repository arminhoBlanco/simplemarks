<?php
require '../vendor/autoload.php';

session_start();

if (!isset($_SESSION['loggedin'])) {
    header('Location: index.php');
    exit;
}

use Project\Student;

$filename = $_FILES["file"]["name"];
$info = new SplFileInfo($filename);
$extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

if ($extension == 'csv') {
    $student = new Student();
    $filename = $_FILES['file']['tmp_name'];
    $handle = fopen($filename, "r");

    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $student->importCSV($_SESSION['id'], $_SESSION['id_year'], $data);
    }
    fclose($handle);
    header("Location: students.php");
} else {
    $_SESSION['message'] = '
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    El archivo no es un fichero <strong>.csv</strong>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>';
    header("Location: import_students_form.php");
}
