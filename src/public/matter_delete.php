<?php
require '../vendor/autoload.php';

session_start();

use Project\Matter;

$matter = new Matter();

$name = $matter->getNameMatter($_SESSION['id'], $_SESSION['id_year'], $_GET['id_matter']);
$matter->deleteMatter($_SESSION['id'], $_SESSION['id_year'], $_GET['id_matter']);

$_SESSION['message'] = '
<div class="alert alert-success alert-dismissible fade show" role="alert">
    Materia (' . $name->grade . ') ' . $name->name . ' <strong>eliminada</strong>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>';

header("Location: year.php");
