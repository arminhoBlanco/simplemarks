<?php
require '../vendor/autoload.php';

session_start();

if (!isset($_SESSION['loggedin'])) {
    header('Location: index.php');
    exit;
}

use Philo\Blade\Blade;
use Project\Student;
use Project\Year;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$year = (new Year())->getNameYears($_SESSION['id'], $_SESSION['id_year']);
$student = (new Student())->getInfoStudent($_SESSION['id'], $_SESSION['id_year'], $_GET['id_student']);

$message = null;

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

echo $blade->view()->make('student', compact('year', 'student', 'message'))->render();
