<?php
require '../vendor/autoload.php';

session_start();

use Project\Account;

if (!isset($_POST['email'], $_POST['password'])) {
    exit('Please fill both the email and password fields!');
}

$account = (new Account())->getAccount($_POST['email']);

if (count($account)) {
    if (password_verify($_POST['password'], $account[0]->password)) {
        session_regenerate_id();
        $_SESSION['loggedin'] = TRUE;
        $_SESSION['name'] = $_POST['email'];
        $_SESSION['id'] = $account[0]->id;
        header('Location: ./home.php');
    } else {
        $_SESSION['message'] = '
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Correo electrónico y/o contraseña <strong>incorrecta</strong>.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>';
        header('Location: login.php');
    }
} else {
    $_SESSION['message'] = '
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Correo electrónico y/o contraseña <strong>incorrecta</strong>.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>';
    header('Location: login.php');
}
