<?php
require '../vendor/autoload.php';

session_start();

if (!isset($_SESSION['loggedin'])) {
    header('Location: index.php');
    exit;
}

use Philo\Blade\Blade;
use Project\Year;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$years = (new Year())->getAllYears($_SESSION['id']);
$message = null;

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

echo $blade->view()->make('home', compact('years', 'message'))->render();
