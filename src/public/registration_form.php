<?php
require '../vendor/autoload.php';

session_start();

if (isset($_SESSION['loggedin'])) {
    header('Location: home.php');
    exit;
}

use Philo\Blade\Blade;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$message = null;

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

echo $blade->view()->make('registration', compact('message'))->render();
