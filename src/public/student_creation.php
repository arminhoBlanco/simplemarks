<?php
require '../vendor/autoload.php';

session_start();

use Project\Student;

(new Student())->createStudent($_SESSION['id'], $_SESSION['id_year'], $_POST);

$_SESSION['message'] = '
<div class="alert alert-success alert-dismissible fade show" role="alert">
    Estudiante (' . $_POST['student_first_name'] . ') ' . $_POST['student_last_name'] . ' <strong>creado</strong>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>';

header("Location: students.php");
