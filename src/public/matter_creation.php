<?php
require '../vendor/autoload.php';

session_start();

use Project\Matter;

(new Matter())->createMatter($_POST, $_SESSION['id'], $_SESSION['id_year']);

$_SESSION['message'] = '
<div class="alert alert-success alert-dismissible fade show" role="alert">
    Materia (' . $_POST['matter_grade'] . ') ' . $_POST['matter_name'] . ' <strong>creada</strong>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>';

header("Location: year.php");
