<?php
require '../vendor/autoload.php';

session_start();

use Project\Year;

(new Year())->createYear($_POST, $_SESSION['id']);

$_SESSION['message'] = '
<div class="alert alert-success alert-dismissible fade show" role="alert">
    Año escolar (' . $_POST['year_college'] . ') ' . $_POST['year_name'] . ' <strong>creado</strong>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>';

header("Location: home.php");
