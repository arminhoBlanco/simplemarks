<?php
require '../vendor/autoload.php';

session_start();

if (isset($_SESSION['loggedin'])) {
    print('hoa');
    header('Location: ./home.php');
    exit;
}

use Philo\Blade\Blade;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

echo $blade->view()->make('index')->render();
