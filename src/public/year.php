<?php
require '../vendor/autoload.php';

session_start();

if (!isset($_SESSION['loggedin'])) {
    header('Location: index.php');
    exit;
}

use Philo\Blade\Blade;
use Project\Year;
use Project\Matter;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

if (isset($_GET['id_year'])) {
    $_SESSION['id_year'] = $_GET['id_year'];
}

$year = (new Year())->getNameYears($_SESSION['id'], $_SESSION['id_year']);
$matters = (new Matter())->getAllMattersYear($_SESSION['id'], $_SESSION['id_year']);

$message = null;

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

echo $blade->view()->make('year', compact('year', 'matters', 'message'))->render();
