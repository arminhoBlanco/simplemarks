<?php
require '../vendor/autoload.php';

session_start();

use Project\Student;

$student = new Student();

$name = $student->getInfoStudent($_SESSION['id'], $_SESSION['id_year'], $_GET['dni']);
$student->deleteStudent($_SESSION['id'], $_SESSION['id_year'], $_GET['dni'],);

$_SESSION['message'] = '
<div class="alert alert-success alert-dismissible fade show" role="alert">
    Estudiante ' . $name->first_name . ' ' . $name->last_name . '  <strong>eliminado</strong>.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>';

header("Location: ./students.php");
