<?php
require '../vendor/autoload.php';

session_start();

use Project\Account;

if (!isset($_POST['email'], $_POST['password1'], $_POST['password2'])) {
    exit('Please fill both the email and password fields!');
}

if ($_POST['password1'] != $_POST['password2']) {
    $_SESSION['message'] = '
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        La contraseña <strong>no coninciden</strong>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>';
    header("Location: registration_form.php");
} else {
    $account = (new Account())->getAccount($_POST['email']);
    if (count($account)) {
        $_SESSION['message'] = '
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            El <strong>email</strong> ya se encuentra en uso.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>';
        header("Location: registration_form.php");
    } else {
        $_SESSION['message'] = '
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            Cuenta creada <strong>satisfactoriamente</strong>.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>';
        $encriptPass = password_hash($_POST['password1'], PASSWORD_DEFAULT);
        (new Account())->createAccount($_POST['email'], $encriptPass);
        header("Location: login.php");
    }
}
