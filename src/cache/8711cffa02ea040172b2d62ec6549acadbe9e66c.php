
<?php $__env->startSection('head'); ?>
<style>
    :root {
        --input-padding-x: 1.5rem;
        --input-padding-y: .75rem;
    }

    body {
        background: #007bff;
        background: linear-gradient(to right, #0062E6, #33AEFF);
    }

    .card-registration {
        border: 0;
        border-radius: 1rem;
        box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
    }

    .card-registration .card-title {
        margin-bottom: 2rem;
        font-weight: 300;
        font-size: 1.5rem;
    }

    .card-registration .card-body {
        padding: 2rem;
    }

    .form-registration {
        width: 100%;
    }

    .form-registration .btn {
        font-size: 80%;
        border-radius: 5rem;
        letter-spacing: .1rem;
        font-weight: bold;
        padding: 1rem;
        transition: all 0.2s;
    }

    .form-label-group {
        position: relative;
        margin-bottom: 1rem;
    }

    .form-label-group input {
        height: auto;
        border-radius: 2rem;
    }

    .form-label-group>input,
    .form-label-group>label {
        padding: var(--input-padding-y) var(--input-padding-x);
    }

    .form-label-group>label {
        position: absolute;
        top: 0;
        left: 0;
        display: block;
        width: 100%;
        margin-bottom: 0;
        /* Override default `<label>` margin */
        line-height: 1.5;
        color: #495057;
        border: 1px solid transparent;
        border-radius: .25rem;
        transition: all .1s ease-in-out;
    }

    .form-label-group input::-webkit-input-placeholder {
        color: transparent;
    }

    .form-label-group input:-ms-input-placeholder {
        color: transparent;
    }

    .form-label-group input::-ms-input-placeholder {
        color: transparent;
    }

    .form-label-group input::-moz-placeholder {
        color: transparent;
    }

    .form-label-group input::placeholder {
        color: transparent;
    }

    .form-label-group input:not(:placeholder-shown) {
        padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
        padding-bottom: calc(var(--input-padding-y) / 3);
    }

    .form-label-group input:not(:placeholder-shown)~label {
        padding-top: calc(var(--input-padding-y) / 3);
        padding-bottom: calc(var(--input-padding-y) / 3);
        font-size: 12px;
        color: #777;
    }

    .btn-google {
        color: white;
        background-color: #ea4335;
    }

    .btn-facebook {
        color: white;
        background-color: #3b5998;
    }

    /* Fallback for Edge
-------------------------------------------------- */

    @supports (-ms-ime-align: auto) {
        .form-label-group>label {
            display: none;
        }

        .form-label-group input::-ms-input-placeholder {
            color: #777;
        }
    }

    /* Fallback for IE
-------------------------------------------------- */

    @media  all and (-ms-high-contrast: none),
    (-ms-high-contrast: active) {
        .form-label-group>label {
            display: none;
        }

        .form-label-group input:-ms-input-placeholder {
            color: #777;
        }
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('nav'); ?>
<span class="w-100 d-lg-none d-block"></span>
<a href="./index.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./login.php"><i class="fas fa-sign-in-alt"></i> Iniciar sesión</a>
                <a class="dropdown-item" href="./registration_form.php"><i class="fas fa-user-plus"></i> Registrarse</a>
            </div>
        </li>
    </ul>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col">
        <?php if($message): ?>
        <?php echo $message; ?>

        <?php endif; ?>
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-registration my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Registrarse</h5>
                    <form class="form-registration" action="./registration.php" method="POST">
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
                            <label for="inputEmail">Correo electrónico</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="inputPassword1" class="form-control" placeholder="Password" name="password1" onkeyup='check();' required>
                            <label for="inputPassword1">Contraseña</label>
                        </div>
                        <div class="form-label-group">
                            <input type="password" id="inputPassword2" class="form-control" placeholder="Password" name="password2" onkeyup='check();' required>
                            <label for="inputPassword2">Repetir contraseña</label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Registrarse</button>
                        <hr class="my-4">
                        <button class="btn btn-lg btn-google btn-block text-uppercase"><i class="fab fa-google mr-2"></i>Google</button>
                        <button class="btn btn-lg btn-facebook btn-block text-uppercase"><i class="fab fa-facebook-f mr-2"></i>Facebook</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let password1 = document.querySelector('#inputPassword1');
    let password2 = document.querySelector('#inputPassword2');
    let btnSubmit = document.querySelector('button[type="submit"]');

    var check = function() {
        btnSubmit.classList.remove('btn-primary');
        btnSubmit.classList.remove('btn-success');
        btnSubmit.classList.remove('btn-danger');
        btnSubmit.disabled = true;
        btnSubmit.innerHTML = "";
        if (password1.value == "" && password2.value == "") {
            btnSubmit.classList.add('btn-primary');
            btnSubmit.innerHTML = "Registrarse";
        } else {
            if (password1.value == password2.value) {
                btnSubmit.classList.add('btn-success');
                btnSubmit.disabled = false;
                btnSubmit.innerHTML = "Registrarse";
            } else {
                btnSubmit.classList.add('btn-danger');
                btnSubmit.disabled = true;
                btnSubmit.innerHTML = "No coincide";
            }
        }
    }

    window.onload(check());
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/2DAWB/sandbox/public/SimpleMarks/views/registration.blade.php ENDPATH**/ ?>