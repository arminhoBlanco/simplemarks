
<?php $__env->startSection('head'); ?>
<style>
    .sticky-offset {
        top: 80px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('nav'); ?>
<span class="w-100 d-lg-none d-block"></span>
<a href="./home.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./logout.php"><i class="fas fa-sign-out-alt"></i>Cerrar sesión</a>
            </div>
        </li>
    </ul>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12">
                <a class="btn btn-primary btn-lg btn-block d-md-inline" href="./students.php" role="button">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col">
        <h2>¿Qué es un archivo .csv?</h2>
        <hr>
        <p class="text-justify">
            Las siglas CSV vienen del inglés "Comma Separated Values" y significan valores separados por comas. Dicho esto, un archivo CSV es cualquier archivo de texto en el cual los caracteres están separados por comas, haciendo una especie de tabla en filas y columnas. Las columnas quedan definidas por cada punto y coma (,), mientras que cada fila se define mediante una línea adicional en el texto. De esta manera, se pueden crear archivos CSV con gran facilidad (lo explicamos más adelante). Es por esto que los archivos .csv están asociados directamente a la creación de tablas de contenido.
        </p>
        <h2>Como usarlo en nuestra página</h2>
        <hr>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="list-group position-sticky position-md-static sticky-offset" id="list-tab" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Crear un archivo .csv</a>
                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Importarlo</a>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                        <h4>
                            Nos situamos en una carpeta de Windows y creamos un archivo de texto.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/01.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <h4>
                            Lo llamamos <strong>alumnos</strong>.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/02.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <h4>
                            Escribimos los datos de los alumnos en el siguiente orden, separados por (,):
                        </h4>
                        <hr>
                        <ol>
                            <li>DNI</li>
                            <li>Nombre</li>
                            <li>Apellidos</li>
                            <li>Grupo</li>
                        </ol>
                        <p class="text-center">
                            <img src="./img/03.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <h4>
                            Lo guardamos como archivo <strong>.csv</strong>.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/04.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <p>
                            Escribimos al final del nombre del archivo <strong>.csv</strong>
                        </p>
                        <p class="text-center">
                            <img src="./img/05.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <h4>
                            Vemos como se nos creo un archivo <strong>.csv</strong>.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/06.png" class="img-fluid" alt="Responsive image">
                        </p>
                    </div>
                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                        <h4>
                            En la página web pulsamos el boton <strong>Seleccionar archivo</strong>.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/07.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <h4>
                            Seleccionamos el archivo <strong>.csv</strong> que creamos anteriormente.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/08.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <h4>
                            Pulsamos el boton <strong>Subir</strong>.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/09.png" class="img-fluid" alt="Responsive image">
                        </p>
                        <h4>
                            Vemos como los alumnos se importaron a tu perfil.
                        </h4>
                        <hr>
                        <p class="text-center">
                            <img src="./img/10.png" class="img-fluid" alt="Responsive image">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row text-center">
    <div class="col-12">
        <?php if($message): ?>
        <?php echo $message; ?>

        <?php endif; ?>
        <form enctype="multipart/form-data" method="post" action="./import_student.php">
            <input type="file" name="file" id="file" class="btn btn-warning btn-lg btn-block d-md-inline">
            <button type=" submit" class="btn btn-primary mt-2">Subir</button>
            <button type="reset" class="btn btn-success mt-2">Limpiar</button>
            <a href="./students.php" class="btn btn-info mt-2" role="button" aria-pressed="true">Volver</a>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/2DAWB/sandbox/public/SimpleMarks/views/import-students.blade.php ENDPATH**/ ?>