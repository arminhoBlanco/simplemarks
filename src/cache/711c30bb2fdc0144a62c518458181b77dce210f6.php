
<?php $__env->startSection('nav'); ?>
<span class="w-100 d-lg-none d-block"></span>
<a href="./index.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./login.php"><i class="fas fa-sign-in-alt"></i> Iniciar sesión</a>
                <a class="dropdown-item" href="./registration_form.php"><i class="fas fa-user-plus"></i> Registrarse</a>
            </div>
        </li>
    </ul>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col">
        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(88).jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(121).jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(31).jpg" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <ol class="carousel-indicators">
                <li data-target="#carousel-thumb" data-slide-to="0" class="active">
                </li>
                <li data-target="#carousel-thumb" data-slide-to="1">
                </li>
                <li data-target="#carousel-thumb" data-slide-to="2">
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="row mt-5">
    <div class="col-12 col-lg-5">
        <img src="https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/480/public/media/image/2018/06/como-hacer-grafico-doble-eje-excel.jpg?itok=gYbgT5YW" class="w-100 rounded m-auto" alt="Grafic Excel">
    </div>
    <div class="col">
        <p class="text-justify">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
        </p>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <p class="text-justify">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est rerum modi unde culpa sapiente nihil maxime, voluptatibus, molestiae facere laboriosam molestias, hic doloremque. Sint tempore ex facilis quia placeat consequuntur!
        </p>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/2DAWB/sandbox/public/SimpleMarks/views/index.blade.php ENDPATH**/ ?>