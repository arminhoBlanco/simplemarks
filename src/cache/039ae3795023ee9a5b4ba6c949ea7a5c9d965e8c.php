
<?php $__env->startSection('nav'); ?>
<span class="w-100 d-lg-none d-block"></span>
<a href="./home.php" class="text-light navbar-brand abs my-auto align-middle offset-2 offset-sm-4 offset-lg-0" id="head_simplemarks">SimpleMarks</a>
<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSession" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSession">
                <a class="dropdown-item" href="./logout.php"><i class="fas fa-sign-out-alt"></i>Cerrar sesión</a>
            </div>
        </li>
    </ul>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-12">
        <a class="btn btn-primary btn-lg btn-block d-md-inline" href="./students.php" role="button">
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>
</div>
<div class="row mt-4">
    <div class="col">
        <form action="student_creation.php" method="POST">
            <div class="form-group">
                <label for="student_dni">DNI</label>
                <input type="text" class="form-control" id="student_dni" placeholder="00000000A" name="student_dni" required>
            </div>
            <div class="form-group">
                <label for="student_first_name">Nombre</label>
                <input type="text" class="form-control" id="student_first_name" placeholder="Jesus Ándres" name="student_first_name" required>
            </div>
            <div class="form-group">
                <label for="student_last_name">Apellidos</label>
                <input type="text" class="form-control" id="student_last_name" placeholder="Perez Gonzales" name="student_last_name" required>
            </div>
            <div class="form-group">
                <label for="student_grade">Grupo</label>
                <input type="text" class="form-control" id="student_grade" placeholder="2 DAW  B" name="student_grade" required>
            </div>
            <button type=" submit" class="btn btn-primary">Crear</button>
            <button type="reset" class="btn btn-success">Limpiar</button>
            <a href="./students.php" class="btn btn-info" role="button" aria-pressed="true">Volver</a>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/2DAWB/sandbox/public/SimpleMarks/views/create-student.blade.php ENDPATH**/ ?>